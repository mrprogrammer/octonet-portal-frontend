{
  "parser": "babel-eslint",
  "plugins": ["react", "prettier"],
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true,
      "modules": true
    }
	},
	"env": {
		"browser": true,
		"node": true,
		"jasmine": true,
		"commonjs": true,
		"es6": true
	},

  "settings": {
    "react": {
      "pragma": "React",
      "version": "detect"
		}
	},
  "extends": ["eslint:recommended", "plugin:react/recommended", "airbnb-base", "prettier", "prettier/react"],
  "rules": {
    "arrow-body-style": "warn",
    "class-methods-use-this": "off",
    "global-require": "off",
    "react/jsx-uses-react": 2,
    "react/jsx-uses-vars": 2,
		"react/react-in-jsx-scope": 2,
		"prettier/prettier": "error",
		"react/prop-types": "off"
  }
}
