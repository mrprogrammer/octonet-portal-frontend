import React from 'react'
import { Grid, Typography, Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import { Image, LocalMovies, MenuBook } from '@material-ui/icons'

import { Item } from '.'

const useStyles = makeStyles(() => ({
  title: {
    fontWeight: 'bold',
    marginLeft: 5,
  },
  height: {
    height: 45,
  },
  titleBox: {
    margin: 10,
  },
}))

function Home() {
  const classes = useStyles()

  return (
    <Grid container spacing={32}>
      <Grid item xs={12}>
        <div className={clsx(classes.titleBox, 'rowSpace')}>
          <div className="row">
            <Image />
            <Typography className={classes.title}>آخرین تصاویر</Typography>
          </div>
          <Button size="small">بیشتر</Button>
        </div>
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12}>
        <div className={classes.height} />
        <div className={clsx(classes.titleBox, 'rowSpace')}>
          <div className="row">
            <LocalMovies />
            <Typography className={classes.title}>آخرین کلیپ ها</Typography>
          </div>
          <Button size="small">بیشتر</Button>
        </div>
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12}>
        <div className={classes.height} />
        <div className={clsx(classes.titleBox, 'rowSpace')}>
          <div className="row">
            <MenuBook />
            <Typography className={classes.title}>آخرین مطالب</Typography>
          </div>
          <Button size="small">بیشتر</Button>
        </div>
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
      <Grid item xs={12} md={3}>
        <Item />
      </Grid>
    </Grid>
  )
}

export default Home
