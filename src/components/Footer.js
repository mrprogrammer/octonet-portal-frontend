import React from 'react'
import { Typography, Grid } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

import facebookIcon from '../assets/images/social/facebook.svg'
import instagramIcon from '../assets/images/social/instagram.svg'
import linkedinIcon from '../assets/images/social/linkedin.svg'
import telegramIcon from '../assets/images/social/telegram.svg'
import twitterIcon from '../assets/images/social/twitter.svg'
import whatsappIcon from '../assets/images/social/whatsapp.svg'

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.primary.dark,
    boxShadow: '0px 0px 5px #424242',
    padding: 15,
    marginTop: 15,
  },
  socialIcon: {
    width: 20,
    height: 20,
    marginLeft: 10,
    float: 'right',
  },
}))

function Footer() {
  const classes = useStyles()

  return (
    <Grid container justify="space-between" className={classes.root}>
      <Grid item xs={12} md={6}>
        <Typography>تمامی حقوق برای خودمون محفوظ است</Typography>
      </Grid>
      <Grid item xs={12} md={6}>
        <a href="#">
          <img
            src={facebookIcon}
            className={classes.socialIcon}
            alt="socialIcon"
          />
        </a>
        <a href="#">
          <img
            src={instagramIcon}
            className={classes.socialIcon}
            alt="socialIcon"
          />
        </a>
        <a href="#">
          <img
            src={linkedinIcon}
            className={classes.socialIcon}
            alt="socialIcon"
          />
        </a>
        <a href="#">
          <img
            src={telegramIcon}
            className={classes.socialIcon}
            alt="socialIcon"
          />
        </a>
        <a href="#">
          <img
            src={twitterIcon}
            className={classes.socialIcon}
            alt="socialIcon"
          />
        </a>
        <a href="#">
          <img
            src={whatsappIcon}
            className={classes.socialIcon}
            alt="socialIcon"
          />
        </a>
      </Grid>
    </Grid>
  )
}

export default Footer
