import React from 'react'
import { Container, CssBaseline } from '@material-ui/core'
import { GlobalCss, Bar, Footer } from '.'

function Layout(props) {
  return (
    <>
      <CssBaseline />
      <GlobalCss />
      <Bar />
      <Container>{props.children}</Container>
      <Footer />
    </>
  )
}

export default Layout
