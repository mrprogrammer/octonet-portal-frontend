import { withStyles } from '@material-ui/core/styles'

const GlobalCss = withStyles((theme) => ({
  '@global': {
    body: {
      backgroundColor: '#f6f6f6',
    },
    'a, Link': {
      textDecoration: 'none',
    },
    'a:active, a:focus': {
      outline: 0,
      border: 'none',
    },
    '.center': {
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex',
    },
    '.row': {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      flexWrap: 'wrap',
    },
    '.rowSpace': {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      flexWrap: 'wrap',
      [theme.breakpoints.down('md')]: {
        flexDirection: 'column',
      },
    },
  },
}))(() => null)

export default GlobalCss
