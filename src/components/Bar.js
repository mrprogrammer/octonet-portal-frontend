import React from 'react'
import PropTypes from 'prop-types'
import { fade, makeStyles } from '@material-ui/core/styles'
import {
  AppBar,
  Typography,
  Toolbar,
  IconButton,
  InputBase,
  Badge,
  MenuItem,
  Menu,
  Button,
  useScrollTrigger,
  Slide,
} from '@material-ui/core'
import {
  Search,
  AccountCircle,
  Mail,
  Notifications,
  MoreVert,
} from '@material-ui/icons'
import { Link } from 'react-router-dom'

import iconImage from '../assets/images/icon.svg'

function HideOnScroll(props) {
  const { children, window } = props
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({ target: window ? window() : undefined })

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  )
}

HideOnScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
}

const useStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  logo: {
    width: 40,
    height: 40,
  },
  loginBtnText: {
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  clearHeight: {
    height: 80,
  },
}))

export default function Bar(props) {
  const classes = useStyles()

  const [anchorEl, setAnchorEl] = React.useState(false)
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(false)

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(false)
  }

  const handleMenuClose = () => {
    setAnchorEl(false)
    handleMobileMenuClose()
  }

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget)
  }

  const menuId = 'primary-search-account-menu'
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={anchorEl}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>حساب کاربری</MenuItem>
      <MenuItem onClick={handleMenuClose}>خروج از اکانت</MenuItem>
    </Menu>
  )

  const mobileMenuId = 'primary-search-account-menu-mobile'
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={mobileMoreAnchorEl}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton color="inherit">
          <Badge badgeContent={4} color="secondary">
            <Mail />
          </Badge>
        </IconButton>
        <p>پیام های شخصی</p>
      </MenuItem>
      <MenuItem>
        <IconButton color="inherit">
          <Badge badgeContent={11} color="secondary">
            <Notifications />
          </Badge>
        </IconButton>
        <p>اطاعیه ها</p>
      </MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton aria-controls="primary-search-account-menu" color="inherit">
          <AccountCircle />
        </IconButton>
        <p>پروفایل</p>
      </MenuItem>
    </Menu>
  )

  const renderButtonUser = (
    <>
      <div className={classes.sectionDesktop}>
        <IconButton color="inherit">
          <Badge badgeContent={4} color="secondary">
            <Mail />
          </Badge>
        </IconButton>
        <IconButton color="inherit">
          <Badge badgeContent={17} color="secondary">
            <Notifications />
          </Badge>
        </IconButton>
        <Button
          edge="end"
          aria-controls={menuId}
          onClick={handleProfileMenuOpen}
          color="inherit"
        >
          <AccountCircle />
          <Typography>حسین شفیعیان</Typography>
        </Button>
      </div>
      <div className={classes.sectionMobile}>
        <IconButton
          aria-controls={mobileMenuId}
          onClick={handleMobileMenuOpen}
          color="inherit"
        >
          <MoreVert />
        </IconButton>
      </div>
    </>
  )

  const renderButtonLogin = (
    <>
      <Button variant="outlined" color="#fff">
        <AccountCircle />
        <Typography className={classes.loginBtnText}>
          &nbsp; ورود / عضویت
        </Typography>
      </Button>
    </>
  )

  return (
    <div className={classes.grow}>
      {renderMobileMenu}
      {renderMenu}
      <HideOnScroll {...props}>
        <AppBar>
          <Toolbar>
            <Link to="/">
              <img
                src={iconImage}
                className={classes.logo}
                alt="logo"
                title="logo"
              />
            </Link>
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <Search />
              </div>
              <InputBase
                placeholder="جست و جو ..."
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
              />
            </div>
            <div className={classes.grow} />
            {false ? renderButtonUser : renderButtonLogin}
          </Toolbar>
        </AppBar>
      </HideOnScroll>
      <div className={classes.clearHeight} />
    </div>
  )
}
