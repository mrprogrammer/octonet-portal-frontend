import React from 'react'
import { CircularProgress } from '@material-ui/core'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'

import { Layout } from '../components'
import { routes, privateRoutes } from './routes'

function Loading() {
  return (
    <div className="center" style={{ minHeight: '600px' }}>
      <CircularProgress
        disableShrink
        size={40}
        thickness={4}
        color="secondary"
      />
    </div>
  )
}

function Router() {
  return (
    <BrowserRouter>
      <Layout>
        <React.Suspense fallback={<Loading />}>
          <Switch>
            {routes.map((route) => (
              <Route key={route.path} {...route} />
            ))}
            {privateRoutes.map((route) => (
              <Route key={route.path} {...route} />
            ))}
            <Redirect to="/" />
          </Switch>
        </React.Suspense>
      </Layout>
    </BrowserRouter>
  )
}

export default Router
