import React from 'react'

const Home = React.lazy(() => import('../pages/home/Home'))
const About = React.lazy(() => import('../pages/about/About'))

const routes = [
  {
    path: '/',
    component: Home,
    exact: true,
  },
  {
    path: '/about',
    component: About,
    exact: true,
  },
]

const privateRoutes = [
  {
    path: '/profile',
    component: Home,
    // exact: true,
    private: true,
  },
]

export { privateRoutes, routes }
