import React from 'react'
import {
  createMuiTheme,
  MuiThemeProvider,
  StylesProvider,
  jssPreset,
  createGenerateClassName,
} from '@material-ui/core/styles'
import { CssBaseline } from '@material-ui/core'
import { create } from 'jss'
import rtl from 'jss-rtl'

import Router from './router/Router'
import './assets/fonts/IRANSans.css'

// CreateMuiTheme
const theme = createMuiTheme({
  direction: 'rtl',
  palette: {
    primary: { main: '#1abc9c' },
    secondary: { main: '#e67e22' },
  },
  typography: {
    useNextVariants: true,
    fontFamily: ['is', 'tahoma'].join(','),
  },
})

// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] })

// Custom Material-UI class name generator.
const generateClassName = createGenerateClassName({
  dangerouslyUseGlobalCSS: false,
  productionPrefix: 'c',
})

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <StylesProvider jss={jss} generateClassName={generateClassName}>
        <Router />
      </StylesProvider>
    </MuiThemeProvider>
  )
}

export default App
